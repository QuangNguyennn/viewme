import request from '@/utils/request'

export function getFeed() {
    return request({
        url: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/450744/mock-data.json',
        method: 'get',
    })
}