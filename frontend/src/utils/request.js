import axios from 'axios'
import store from '@/store'

// create an axios instance
const service = axios.create({
    baseURL: ``, // '/api' backend url
    //withCredentials: true, // send cookies when cross-domain requests
    timeout: 5000 // request timeout
})


// request interceptor
service.interceptors.request.use(
    config => {
        // set request header
        if (store.getters.token) {
            config.headers['Authorization'] = 'Bearer ' + getToken()
            config.headers['Content-Type'] = "application/json"
            config.headers['Access-Control-Allow-Origin'] = "*"
        }
        config.headers['Content-Type'] = "text/plain;charset=utf-8"
        config.headers['Access-Control-Allow-Origin'] = "*"
        return config
    },
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {
        const res = response.data

        // if the custom code is not 20000, it is judged as an error.

        //for testing
        return res

        if (res.code !== 20000) {

            console.log(res)

            // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
            if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
                // to re-login
                MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
                    confirmButtonText: 'Re-Login',
                    cancelButtonText: 'Cancel',
                    type: 'warning'
                }).then(() => {
                    store.dispatch('user/resetToken').then(() => {
                        location.reload()
                    })
                })
            }
            return Promise.reject(new Error(res.message || 'Error'))
        } else {
            return res
        }
    },
    error => {
        console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)

export default service


// export default {
//     hello() {
//         return AXIOS.get(`/hello`);
//     },
//     getUser(userId) {
//         return AXIOS.get(`/user/` + userId);
//     },
//     createUser(firstName, lastName) {
//         return AXIOS.post(`/user/` + firstName + '/' + lastName);
//     },
//     getSecured(user, password) {
//         return AXIOS.get(`/secured/`, {
//             auth: {
//                 username: user,
//                 password: password
//             }
//         });
//     },
//     getFakeData() {
//         return AXIOS.get("https://s3-us-west-2.amazonaws.com/s.cdpn.io/450744/mock-data.json");
//     }
// }