const state = {
    permission_routes: []
}

const mutations = {
    SET_PERMISSION_ROUTES: (state, permission_routes) => {
        state.permission_routes = permission_routes
    },

}

const actions = {

}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}