import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueLogger from 'vuejs-logger';
import enLang from 'element-ui/lib/locale/lang/en'
import VueI18n from 'vue-i18n'


// ENV check
const isProduction = process.env.NODE_ENV === 'production';

// Logger
const options = {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
};

// Logger
Vue.use(VueLogger, options);

// Tips
Vue.config.productionTip = isProduction;

// Element UI
Vue.use(ElementUI);

//Locale
Vue.use(VueI18n)

new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store,
    components: { App }
}).$mount('#app');